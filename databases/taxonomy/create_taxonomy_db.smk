# Create Taxonomy database and mappings #########################################

# RUN: snakemake -s create_taxonomy_db.sm

# import modules

#### set variables #############################################################

#### rule all ##################################################################

rule all:
    input: "meta_database.db"

#### rules #####################################################################

rule unpack_taxonomy:
    input: "meta_database.fa.gz"
    output: "meta_database.fasta"
    shell: "gunzip -c {input} > {output}"

rule build_index_taxonomy:
    input: "meta_database.fasta"
    output: "meta_database.db"
    conda: "envs/taxonomy.yml"
    shell: "prerapsearch -d {input} -n {output}"

################################################################################
