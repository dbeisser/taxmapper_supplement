# Create KEGG pathway mapping (only with license) ##############################

# RUN: snakemake -s create_kegg_mapping.sm
# TODO: include KEGG HMM search

# rule all #####################################################################

rule all:
    input: "ko2pathway.h5" if config["KEGG_pathways"] != "" else []

#workdir: "../databases/uniprot/"
# rules ########################################################################

if config["KEGG_pathways"] != "":
    rule copy_pathways:
        input: config["KEGG_pathways"]
        output: "ko2pathway.txt"
        shell: """
                cp {input} {output};
                sed -i 's/ko://g' {output};
                sed -i 's/path://g' {output}
            """

    rule hdf5_pathway_mapping:
        input: "ko2pathway.txt"
        output: "ko2pathway.h5"
        conda: "envs/uniprot.yml"
        script: "scripts/hdf5_pathway_mapping.py"

################################################################################
