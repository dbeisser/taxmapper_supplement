import time

import Bio.KEGG.REST as krest
import sys

#def second_ret(*args):
args=list(sys.argv)
file_name="tmp_kegg2ko_"+str(args[1])
args=args[2].strip().split(";")

chunks = [args[i:i + 100] for i in range(0, len(args), 100)]

#counter = 0
ret_list=[]
for chunk in chunks:
    #counter +=1
    string="+".join(chunk)
    try:
        for i in krest.kegg_link("ko", string):
            ret_list.append(i.strip())
    except:
        time.sleep(3)
        for i in krest.kegg_link("ko", string):
            ret_list.append(i.strip())
fs=open(file_name,"a+")
for line in set(ret_list):
    fs.write(str(line)+"\n")
fs.close()