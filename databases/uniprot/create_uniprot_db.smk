# Create Uniprot database and mappings #########################################

# RUN: snakemake -s create_uniprot_db.sm

# import modules ###############################################################

from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
from collections import defaultdict

# set variables ################################################################

# FTP Uniprot
FTP = FTPRemoteProvider()
UNIPROTID = "ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping.dat.gz"
UNIPROTDB = "ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz"
UNIPROTID2 = "/mnt/biodiv/Manan/Gruga_21/Manan_gw21mtt/taxmapper_supplement/databases/uniprot/idmapping.dat.gz" #### not working

# rule all #####################################################################

rule all:
    input: ["uniprot2ko.h5", "uniprot2gene.h5","uniprot_sprot.db"] if config["KO_list_file"] != "" else ["uniprot2gene.h5","uniprot_sprot.db"]

# rules ########################################################################

# download new Uniprot database
rule download1:
    input:
        id = FTP.remote(UNIPROTID, static=True)
    output:
        id = "idmapping.dat.gz"
    shell:
        """
            cp {input.id} {output.id};

        """

rule download2:
    input:
        db = FTP.remote(UNIPROTDB, static=True)
    output:
        db = "uniprot_sprot.fasta.gz"
    shell:
        """
            mv {input.db} {output.db};
        """

rule get_KOs:
    input: "idmapping.dat.gz", config["KO_list_file"]
    output: "uniprot2ko.txt","uniprot2kegg.txt"
    script: "scripts/uniprot2koMapper.py"

rule get_gene_symbols:
    input: "idmapping.dat.gz"
    output: "uniprot2gene.txt"
    shell:
        """
            echo -e 'Entry\tGene name' > {output};
            zcat {input} | grep -P '\tGene_Name\t' | cut -f 1,3 >> {output};
        """

rule hdf5_KO_mapping:
    input: "uniprot2ko.txt"
    output: "uniprot2ko.h5"
    conda: "envs/uniprot.yml"
    script: "scripts/hdf5_KO_mapping.py"

rule hdf5_gene_mapping:
    input: "uniprot2gene.txt"
    output: "uniprot2gene.h5"
    conda: "envs/uniprot.yml"
    script: "scripts/hdf5_gene_mapping.py"

rule unpack_uniprot:
    input: "uniprot_sprot.fasta.gz"
    output: "uniprot_sprot.fasta"
    shell: "gunzip -c {input} > {output}"

rule build_index_uniprot:
    input: "uniprot_sprot.fasta"
    output: "uniprot_sprot.db"
    conda: "envs/uniprot.yml"
    shell: "prerapsearch -d {input} -n {output}"

################################################################################
