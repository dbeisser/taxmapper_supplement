import pandas as pd
import subprocess

assembly_req=snakemake.params["assembly"]
tax_file=snakemake.input["tax"]
read_counts_file=snakemake.input["reads"]
output_file=snakemake.output[0]


if not assembly_req:
    subprocess.call(["cp", tax_file, output_file])
else:
    read_quant=pd.read_table(read_counts_file, index_col="Name")
    with open(output_file, 'wt') as f:
        with open(tax_file, 'rt') as g:
            for lineTax in g:
                t_name=lineTax.strip().split()[0]
                counts = int(round(read_quant.at[t_name,"NumReads"]))
                for i in range(0,counts):
                    f.write(lineTax)
