import subprocess

if len(snakemake.input["reads"])==2:
    subprocess.call(["taxmapper", "search", "-f", snakemake.input["reads"][0], "-r", snakemake.input["reads"][1], "-o", snakemake.params["out"], "-t", str(snakemake.threads), "-d", snakemake.input["db"]])
else:
    subprocess.call(["taxmapper", "search", "-f", snakemake.input["reads"][0], "-o", snakemake.params["out"], "-t", str(snakemake.threads), "-d", snakemake.input["db"]])
