library(edgeR)
library(vegan)
library(ggplot2)
library(reshape2)
library(gridExtra)
snakemake@source("ordination.R")

counts <- read.table(snakemake@input[["counts"]], sep="\t", header=T, row.names=1, check.names=FALSE)
metadata <- read.table(snakemake@input[["groups"]], sep="\t", header=T, row.names=1, check.names=FALSE)

d <- DGEList(counts)
d <- calcNormFactors(d)
counts.norm <- cpm(d, normalized.lib.sizes = T, log=T)
counts.norm <- counts.norm[head(order(apply(counts.norm, 1, var), decreasing = T), 500),]

counts.pca <- rda(t(counts.norm))
counts.coord <- getOrdinationXY.sites(oord=counts.pca, choices=1:2)
counts.gg <- cbind(counts.coord, metadata[rownames(counts.coord),], samples=rownames(counts.coord))
colnames(counts.gg) <- c(colnames(counts.coord), colnames(metadata), "samples")
variance <- eigenvals(counts.pca)/sum(eigenvals(counts.pca))*100
counts.gg <- melt(counts.gg, id = c("samples", "x", "y"))
type <- unlist(lapply(lapply(data.frame(metadata[rownames(counts.coord),]), function(x) is(x)[[1]][1]), rep, dim(data.frame(metadata[rownames(counts.coord),]))[1]))
counts.gg <- cbind(counts.gg, type)
out <- by(data = counts.gg, INDICES = counts.gg$variable, FUN = function(m) {
    if(m$type[1] == "integer" || m$type[1] == "numeric")
    {
        p <- ggplot(m, aes(na.rm=TRUE, fill=as.numeric(value), x=x, y=y))
    }else
    {
        p <- ggplot(m, aes(na.rm=TRUE, fill=value, x=x, y=y))
    }
    p <- p + geom_point(size=5, shape=21, colour = "black")+
    geom_text(aes(label=samples), colour = "black", hjust=1, vjust=2) +
    theme_bw()+
    geom_hline(yintercept=0, linetype="dotted") +
    geom_vline(xintercept=0, linetype="dotted") +
    xlab(paste("PC1 (", round(variance[1],2), "% variance)", sep="" ))+
    ylab(paste("PC2 (", round(variance[2],2), "% variance)", sep="" ))
    if(m$type[1] == "integer" || m$type[1] == "numeric")
    {
        p <- p + scale_fill_gradient2(low="#008000", high="#B20000", mid="yellow", name=m$variable[1])
    }
    if(m$type[1] == "character" || m$type[1] == "factor")
    {
        p <- p + scale_fill_discrete(name=m$variable[1])
    }
    p
})

pdf(snakemake@output[[1]], width=7, height=7*dim(metadata)[2])
do.call(grid.arrange, out)
dev.off()
