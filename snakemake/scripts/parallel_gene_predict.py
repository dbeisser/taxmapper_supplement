import re

import os
from multiprocessing import Pool
import subprocess

test=True
if test:
    sample="C1_S1_R"
    assembly_file="/home/hb0358/Disk2/Disk2/Data/MTT_mbs/tmp_tes_playground/t_3/soft_filtered_transcripts.fasta"
    gmm_executable="/home/hb0358/Disk2/Disk2/Data/MTT_mbs/genemark_hmm_euk_linux_64/ehmm/gmhmme3"
    gmm_model="/home/hb0358/Disk2/Disk2/Data/MTT_mbs/genemark_hmm_euk_linux_64/ehmm/mod/a_thaliana.mod"
    threads=8
else:
    sample = snakemake.params["sample"]
    assembly_file=snakemake.input["assembly"]
    gmm_executable = snakemake.input["gmmhmme_exec"]
    gmm_model = snakemake.input["gmm_model"]
    threads=snakemake.threads
chunks=[]


def cleanup(folder_here,pattern):
    rem_files=find_files(folder_here,pattern)
    for files in rem_files:
        os.remove(files)


def find_files(folder,pattern):
    ret_list=[]
    for file_path in os.listdir(folder):
        # check if current file_path is a file
        if os.path.isfile(os.path.join(folder, file_path)):
            if str(file_path).__contains__(pattern):
                # add filename to list
                ret_list.append(os.path.join(folder, file_path))
    return ret_list


def split_input_files(assembly):
    chunk_size = 2500
    folder = os.path.dirname(assembly_file)

    #clean_before_starting
    cleanup(folder,"tmp")
    cleanup(folder,"_gmhmm_nuc.fna")
    cleanup(folder, "_gmhmm_prot.faa")
    #split file:
    command="awk 'BEGIN {n_seq=0;} /^>/ {if(n_seq%"+str(chunk_size)+"==0){file=sprintf(\""+folder+"/"+sample+"_tmp%d.fa\",n_seq);} print >> file; n_seq++; next;} { print >> file; }' < "+assembly
    os.system(command)
    global chunks
    chunks = find_files(folder, "tmp")


def run_1_chunk(chunk_name):
    #print(f"working on chunk {chunk_name}")
    command=f"{gmm_executable} -m {gmm_model} -p -n {chunk_name}"
    os.system(command)
    output=chunk_name+".lst"
    output_protein=chunk_name+".faa"
    output_neucleotide=chunk_name+".fna"
    protein_flag=False
    neucleotide_flag=False
    protein_sequences_start="# protein sequence of predicted genes"
    protein_sequences_end="# end protein sequence"
    neucleotide_sequences_start="# nucleotide sequence of predicted genes"
    neucleotide_sequences_end="# end nucleotide sequence"
    pout=open(output_protein,"w")
    nout=open(output_neucleotide,"w")
    with open(output,"r") as lst_file:
        for line in lst_file:
            line=line.strip()
            if line.startswith(protein_sequences_end): protein_flag = False
            if line.startswith(neucleotide_sequences_end): neucleotide_flag = False

            #creating Protein_file
            if protein_flag:
                if not line=="":pout.write(line+"\n")
            if line.startswith(protein_sequences_start):protein_flag=True

            #Creating_neucleotide file
            if neucleotide_flag:
                if not line=="":nout.write(line+"\n")
            if line.startswith(neucleotide_sequences_start):neucleotide_flag=True
    pout.close()
    nout.close()


def write_final_fasta_files(file_arr, outfile):
    gene_number=1
    output=open(outfile,"w")
    for file in file_arr:
        with open(file,"r") as filestr:
            for line in filestr:
                line=line.strip()
                if line.startswith(">"):
                    new_line=f">{sample}_gene_{gene_number}|GeneMarkHMM_Euk3|len:{line.split('|')[-1]}\n"
                    #print(new_line)
                    output.write(new_line)
                    gene_number+=1
                else:output.write(line+"\n")
    output.close()


def run_chunks_in_parallel(chunks_here):
    folder = os.path.dirname(chunks_here[0])
    with Pool(threads) as pool: out=pool.map(run_1_chunk,chunks_here)
    neucleotide_files = find_files(folder, "fna")
    # i dont know how or why but just doing the below step sorts the original list, but the new list is empty hence
    # i keep using the original now sorted list in the following step, removing the below unused step
    # will give unsorted file , leaving this in will give the longest genes on top and shortest ones on bottom
    sorted_nuc_list = neucleotide_files.sort(
        key=lambda x: int(x.replace(folder + "/" + sample + "_tmp", "").replace(".fa.fna", "")))
    #print(f"merging {neucleotide_files}")
    output_neuc_file = folder + f"/{sample}_gmhmm_nuc.fna"
    write_final_fasta_files(neucleotide_files, output_neuc_file)
    protein_files = find_files(folder, "faa")
    sorted_prot_list = protein_files.sort(
        key=lambda x: int(x.replace(folder + "/" + sample + "_tmp", "").replace(".fa.faa", "")))
    #print(f"merging {protein_files}")
    output_prot_file = folder + f"/{sample}_gmhmm_prot.faa"
    write_final_fasta_files(protein_files, output_prot_file)
    cleanup(folder,"tmp")




split_input_files(assembly_file)
run_chunks_in_parallel(chunks)
