getOrdinationXY.KO <- function(oord, choices)
{
    xy <- data.frame(scores(oord, choices = choices, display = "sites"), which="Sample")
    xy <- rbind(xy, data.frame(scores(oord, choices = choices, display = "species"), which="KO"))
    colnames(xy) <- c("x", "y", "which")
    return(xy)
}

getOrdinationXY.sites <- function(oord, choices)
{
  xy <- data.frame(scores(oord, choices = choices, display = "sites"))
  colnames(xy) <- c("x", "y")
  return(xy)
}

getOrdinationXY.taxonomy <- function(oord, choices)
{{
  xy <- data.frame(scores(oord, choices = choices, display = "sites"), which="Sample")
  xy <- rbind(xy, data.frame(scores(oord, choices = choices, display = "species"), which="Taxonomy"))
  colnames(xy) <- c("x", "y", "which")
  return(xy)
}}

plotRDA <- function(model)
{
    variance <- eigenvals(model)/sum(eigenvals(model))*100
    sites <- scores(model, display = "sites", choices = c(1,2))
    bp <- scores(model, display = "bp", choices = c(1,2))
    df1 <- data.frame(sites)
    if(!is.null(bp[1][[1]]))
    {
        df2 <- data.frame(bp) * ordiArrowMul(bp)
        names(df2) <- c("x", "y")
    }
    ord.names <- names(df1)
    names(df1) <- c("x", "y")
    q <- ggplot(df1, aes(x=x, y=y)) +
    geom_text(aes(label=rownames(df1)), colour = "#0c4476", hjust=1, vjust=2) +
    geom_point(size=5, shape=21, colour = "black", fill="#0c4476") +
    geom_hline(yintercept=0, linetype="dotted") +
    geom_vline(xintercept=0, linetype="dotted") +
    xlab(paste(ord.names[1], " (", round(variance[1],2), "% variance)", sep="" ))+
    ylab(paste(ord.names[2], " (", round(variance[2],2), "% variance)", sep="" ))+
    theme_bw()
    if(!is.null(bp[1][[1]]))
    {
        q <- q +
        geom_segment(data=df2, aes(x=0, xend=x, y=0, yend=y), color="red", arrow=arrow(length=unit(0.01,"npc"))) +
        geom_text(data=df2, aes(x=x,y=y,label=rownames(df2), hjust=.5*(1-sign(x)),vjust=.5*(1-sign(y))), color="red", size=4)
    }
    q
}
