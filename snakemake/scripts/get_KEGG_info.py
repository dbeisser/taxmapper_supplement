import deepdish as dd
import os

# get additional gene infor via Uniprot ID
dg = dd.io.load(snakemake.input["u2gene"])
dko = dd.io.load(snakemake.input["u2ko"]) if snakemake.input["u2ko"] != [] else{}
# the ko2pathways.h5 can only be created with a license of the KEGG database, the snakefile "databases/uniprot/create_kegg_mapping.py" can be used to create it
if snakemake.input["ko2pw"] != []:
    dp = dd.io.load(snakemake.input["ko2pw"])

inTsv=snakemake.input["tsv"]
with open(inTsv, 'rt') as a, open(snakemake.output[0], 'wt') as c:
    for linea in a:
        linea = linea.rstrip()
        a_vec = linea.split()
        # 0: read, 1: Uniprot ID, 2: log10-evalue, 3: identity, 4: aln-length, 5: sequence
        if a_vec[1] in dg.keys():
            gene = dg[a_vec[1]]
        else:
            gene = "NA"
        if a_vec[1] in dko.keys():
            ko = dko[a_vec[1]]
        else:
            ko = "NA"
        if snakemake.input["ko2pw"] != [] and ko in dp.keys():
            pathways = dp[ko]
        else:
            pathways = ["NA"]
        c.write("\t".join(a_vec)+"\t"+gene+"\t"+ko+"\t"+" ".join(pathways)+"\n")
