import os
import pandas as pd

test = False
if test:
    infiles = sorted((
            "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/tmp_tes_playground/t_4/spades_C2_S4_R/C2_S4_R_ko_scan_result",
            "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/tmp_tes_playground/t_4/spades_C2_S3_R/C2_S3_R_ko_scan_result",
            "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/tmp_tes_playground/t_4/spades_C1_S1_R/C1_S1_R_ko_scan_result",
    ))
    infile_quant = sorted((
        "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/tmp_tes_playground/t_4/spades_C2_S4_R/C2_S4_R_gmhmm_nuc_quant/quant.sf",
        "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/tmp_tes_playground/t_4/spades_C2_S3_R/C2_S3_R_gmhmm_nuc_quant/quant.sf",
        "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/tmp_tes_playground/t_4/spades_C1_S1_R/C1_S1_R_gmhmm_nuc_quant/quant.sf",
    ))
    evalue_threshold = 0.001
    outfile_ko = "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/Statistics_GW21/ko_profile_new_reads.tsv"
    pathway_outfile = outfile_ko.replace("ko_", "ko_pathway_")
    pathways_map = "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/ko/ko_pathway.list"
    kegg_pathway_pedia_file = "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/ko/pathway"
    pathway_pedia_df_file = "/home/hb0358/Disk2/Disk2/Data/MTT_mbs/ko/pathway_substem_map.tsv"

else:
    infiles = snakemake.input["kohmm_results"]
    infile_quant = snakemake.input["gene_read_quant"]
    evalue_threshold = snakemake.params["evalue"]
    outfile_ko = snakemake.output["ko_profile"]
    pathway_outfile = snakemake.output["pathway_profile"]
    pathways_map = snakemake.params["pathway_map"]
    pathway_pedia_df_file = snakemake.output["map_dict"]
    kegg_pathway_pedia_file= snakemake.params["pathway_pedia"]


def create_pathway_pedia():
    pathwa_map_out = pathway_pedia_df_file
    pathway_pedia_Df2 = pd.DataFrame(columns=["subsystem", "pathway_name"])
    pathway_pedia_Df2.index.name = "pathway"

    with open(kegg_pathway_pedia_file, "r") as pfile:
        pathway = ""
        subsystem = ""
        p_name = ""
        entry_num = 0
        entry_flag = False
        for line in pfile:
            line = line.strip()
            if line == "///" and entry_flag:
                if pathway.startswith("map"):
                    pathway_pedia_Df2.at[pathway, "subsystem"] = subsystem
                    pathway_pedia_Df2.at[pathway, "pathway_name"] = p_name
                entry_flag = False
                pathway = ""
                subsystem = ""
                p_name = ""
                # print()
            if line.startswith("ENTRY"):
                entry_flag = True
                entry_num += 1
                entry = line.split()
                pathway = entry[1]
                # print(entry[1].strip(),end="\t")
            if line.startswith("CLASS"):
                if not subsystem == "": print(f"warning multiple classes for {pathway}")
                entry = line.split()
                subsystem = " ".join(entry[1:])
                # print(subsystem,end="")
            if line.startswith("NAME"):
                if not p_name == "": print(f"warning multiple names for {pathway}")
                entry = line.split()
                p_name = " ".join(entry[1:])
                # print(subsystem,end="")
    pathway_pedia_Df2.to_csv(pathwa_map_out, sep="\t")


def create_kegg_pathway_profiles():
    pathway_pedia_df = pd.read_csv(pathway_pedia_df_file, sep="\t", index_col=0)
    kegg_df = pd.DataFrame()
    kegg_pathway_df = pd.DataFrame()
    ko_pathway_map = {}
    # print(pathway_pedia_df)
    with open(pathways_map, "r") as pmap:
        for line in pmap:
            entry = line.strip().split()
            ko = entry[0].split(":")[-1]
            pathway = entry[1].split(":")[-1]
            if ko in ko_pathway_map:
                if pathway.startswith("ko"):
                    pam = pathway.replace("ko", "map")
                    if pam not in ko_pathway_map[ko]: ko_pathway_map[ko].append(pathway)
                else:
                    ko_pathway_map[ko].append(pathway)
            else:
                ko_pathway_map[ko] = [pathway]

    kid_not_found = []
    for infile in infiles:
        sample = infile.split("/")[-1].replace("_ko_scan_result", "")  # geting the sample name from the file name
        kegg_df[sample] = 0
        kegg_pathway_df[sample] = 0
        genes_dict = {}
        print(sample)
        quant_infile = infile.replace("_ko_scan_result_trs_pred.tsv", "_trs_pred_nuc_quant/quant.sf")
        quant_dict = pd.read_csv(quant_infile, sep="\t")
        quant_dict["gene"]=quant_dict["Name"].apply(lambda x: x.split("|")[0])
        quant_dict = pd.DataFrame(quant_dict).set_index("gene")
        with open(infile, 'rt') as g:
            header = next(g)
            for line in g:
                if not line.startswith("#"):
                    line = line[1:].strip().split()
                    line[5] = " ".join(line[5:])
                    line = line[:6]
                    line_eval = float(line[4])  # evalue of the assignament
                    kID = line[1]  # KO ID for the assignament
                    gene_ID = line[0].split("|")[0]
                    line_score = float(line[3])
                    if gene_ID in genes_dict:
                        if line_eval < genes_dict[gene_ID]["ev"] and line_eval < evalue_threshold:
                            genes_dict[gene_ID]["ev"] = line_eval
                            genes_dict[gene_ID]["kid"] = [kID]
                        elif line_eval == genes_dict[gene_ID]["ev"] and line_eval < evalue_threshold:
                            if line_score > genes_dict[gene_ID]["score"]: genes_dict[gene_ID]["kid"].append(kID)
                    else:
                        if line_eval < evalue_threshold:
                            genes_dict[gene_ID] = {"ev": line_eval, "kid": [kID], "score": line_score}
        for gene in genes_dict:
            try:
                read_count = quant_dict.at[gene, "NumReads"]
            except KeyError:
                read_count=0
            #print(read_count)
            for kid in genes_dict[gene]["kid"]:
                if kid in kegg_df.index:
                    # print(f"{kID}\t{sample}\t{nOr}\t{kegg_df.at[kID,sample]}") if kID == "K10693" else print(end="")
                    nv = kegg_df.at[kid, sample] + read_count
                    kegg_df.at[kid, sample] = nv
                else:
                    # print(f"{kID}\t{sample}\t{nOr}")if kID=="K10693" else print(end="")
                    kegg_df.at[kid, sample] = read_count
                if kid in ko_pathway_map:
                    for pathwayS in ko_pathway_map[kid]:

                        if pathwayS in kegg_pathway_df.index:
                            nv = kegg_pathway_df.at[pathwayS, sample] + read_count
                            kegg_pathway_df.at[pathwayS, sample] = nv
                        else:
                            kegg_pathway_df.at[pathwayS, sample] = read_count
                else:
                    kid_not_found.append(kid)
    print(f" WARNING {len(set(kid_not_found))} KiDs not found in pathway map: {set(kid_not_found)}")

    kegg_df = kegg_df.fillna(0)
    kegg_df.to_csv(outfile_ko, sep="\t")
    kegg_pathway_df = kegg_pathway_df.fillna(0)
    kegg_pathway_df = pd.merge(pathway_pedia_df, kegg_pathway_df, left_index=True, right_index=True)
    kegg_pathway_df.index.name = "pathway_ID"
    kegg_pathway_df.to_csv(pathway_outfile, sep="\t")


if not os.path.isfile(pathway_pedia_df_file):create_pathway_pedia()
create_kegg_pathway_profiles()
