from Bio import SeqIO
import pandas as pd

#defining variable
run="not_test" #change to test when testing scipt without snakemake and put input files in the below if section
if run=="test":
    sample = "C1_S1_R"
    assembly_file = f"/home/hb0358/Disk2/Disk2/Data/MTT_mbs/taxmapper_supplement/gruga_samples_t/assembly/spades_{sample}/soft_filtered_transcripts.fasta"#
    quant_file = f"/home/hb0358/Disk2/Disk2/Data/MTT_mbs/taxmapper_supplement/gruga_samples_t/assembly/quant_{sample}/quant.sf"#snakemake.input["quant"]#

    output_seq_and_reads = f"/home/hb0358/Disk2/Disk2/Data/MTT_mbs/taxmapper_supplement/gruga_samples_t/assembly/{sample}_reads_seqs.tsv"#
else:
    # sample="C1_S1_R"
    assembly_file=snakemake.input["assembly"]#f"/home/hb0358/Disk2/Disk2/Data/MTT_mbs/gruga_samples_t/assembly/spades_{sample}/soft_filtered_transcripts.fasta"#
    quant_file=snakemake.input["quant"]#f"/home/hb0358/Disk2/Disk2/Data/MTT_mbs/gruga_samples_t/assembly/quant_{sample}/quant.sf"#snakemake.input["quant"]#

    output_seq_and_reads=snakemake.output[0]#f"/home/hb0358/Disk2/Disk2/Data/MTT_mbs/gruga_samples_t/tmp/{sample}_reads_seqs.tsv"#


#reading files to dataframes:
fasta_file=open(assembly_file)
fasta_dict =SeqIO.to_dict(SeqIO.parse(fasta_file, "fasta"))
read_quant_original=pd.read_table(quant_file, index_col="Name")

read_quant=pd.DataFrame(index=fasta_dict.keys())

#defining function to get number of reads:
def get_read_count(name):
    if name in read_quant_original.index:
        count=read_quant_original.at[name,"NumReads"]
    else:
        count=0
        print(f"warning: {name} not found in the quant.sf file{quant_file}, so adding number of reads for this transcript as 0")
    return count


#mergingn the dataframes
if "Name" not in read_quant.columns : read_quant["Name"]=read_quant.index
if "Seq" not in read_quant.columns : read_quant["Seq"]=read_quant.apply(lambda x:"".join(fasta_dict[x["Name"]].seq), axis=1)
if "NumReads" not in read_quant.columns :
    read_quant["NumReads"]=read_quant["NumReads"]=read_quant.apply(lambda x:get_read_count(x["Name"]), axis=1)
#read_quant_original[read_quant_original["Name"]==read_quant[read_quant["Name"]]]["NumReads"][0]

#writing_outfile
read_quant[["Name","NumReads","Seq"]].to_csv(output_seq_and_reads,index=False, sep = "\t")
