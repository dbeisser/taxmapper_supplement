import subprocess
from Bio import SeqIO
# find average read length, not meaningful in the case of fasta files or assembly files with different sequences lengths.
# But only used to compare between hits of the same sequence.
seqfile=snakemake.input["seq"]
alignmentfile=snakemake.input["align"]
outfile=snakemake.output["tax"]
threads_n=snakemake.threads

if not isinstance(seqfile, str):
    seq = seqfile[0]
else:
    seq = seqfile

comm = "awk '{length_t=length_t+length($3);counter++} END {avg=length_t/(counter-1); print avg}' "+seq
proc = subprocess.Popen(comm, stdout=subprocess.PIPE, shell=True)
readlength_r = proc.communicate()[0]
readlength_r = readlength_r.decode("utf-8").rstrip()
readlength=round(float(readlength_r))

if len(alignmentfile)==2:
    subprocess.call(["taxmapper", "map", "-m", str(readlength), "-f", alignmentfile[0], "-r", alignmentfile[1], "-o", outfile, "-t", str(threads_n)])
else:
    if not isinstance(alignmentfile, str):
        input = alignmentfile[0]
    else:
        input = alignmentfile
    subprocess.call(["taxmapper", "map", "-m", str(readlength), "-f", input, "-o", outfile, "-t", str(threads_n)])
