import pandas as pd
infiles=snakemake.input
evalue=snakemake.wildcards["evalue"]

outfile=snakemake.output[0]

kegg_df=pd.DataFrame()

for infile in infiles:
    sample=infile.split("/")[-1].replace("_gene_annotation.tsv","") #geting the sample name from the file name
    kegg_df[sample]=0
    with open(infile, 'rt') as g:
        for line in g:
            line = line.strip().split("\t")
            line_eval=line[2] #evalue of the assignament
            kID=line[8] #KO ID for teh assignament
            try:
                nOr=round(float(line[5]))#number of reads mapped to this transcript
            except:
                nOr=0
            if float(line_eval) < float(evalue)and kID != "NA":
                if kID in kegg_df.index:
                    #print(f"{kID}\t{sample}\t{nOr}\t{kegg_df.at[kID,sample]}") if kID == "K10693" else print(end="")
                    nv=kegg_df.at[kID,sample]+nOr
                    kegg_df.at[kID,sample]=nv
                else:
                    #print(f"{kID}\t{sample}\t{nOr}")if kID=="K10693" else print(end="")
                    kegg_df.at[kID,sample]=nOr
kegg_df=kegg_df.fillna(0)
kegg_df.to_csv(outfile,sep="\t")