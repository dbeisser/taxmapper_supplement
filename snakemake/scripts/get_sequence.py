from Bio import SeqIO
import pandas as pd

#defining variable
sample="C1_S1_R"
assembly_file=snakemake.input["assembly"]#f"/home/hb0358/Disk2/Disk2/Data/MTT_mbs/gruga_samples_t/assembly/spades_{sample}/soft_filtered_transcripts.fasta"#
type_of_file=snakemake.params["format"]
output_seq_and_reads=snakemake.ouput["counts"]#f"/home/hb0358/Disk2/Disk2/Data/MTT_mbs/gruga_samples_t/tmp/{sample}_reads_seqs.tsv"#

#reading files to dataframes:
fasta_file=open(assembly_file)
fasta_dict =SeqIO.to_dict(SeqIO.parse(fasta_file, type_of_file))

read_quant=pd.DataFrame(index=fasta_dict.keys())

#defining function to get number of reads:
def get_read_count(name):
    count=1
    return count


#mergingn the dataframes
if "Name" not in read_quant.columns : read_quant["Name"]=read_quant.index
if "Seq" not in read_quant.columns : read_quant["Seq"]=read_quant.apply(lambda x:"".join(fasta_dict[x["Name"]].seq), axis=1)
if "NumReads" not in read_quant.columns :
    read_quant["NumReads"]=read_quant["NumReads"]=read_quant.apply(lambda x:get_read_count(x["Name"]), axis=1)
#read_quant_original[read_quant_original["Name"]==read_quant[read_quant["Name"]]]["NumReads"][0]

#writing_outfile
read_quant[["Name","NumReads","Seq"]].to_csv(output_seq_and_reads,index=False, sep = "\t")