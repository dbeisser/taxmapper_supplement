# Join functional and taxonomic results ########################################

# rules ########################################################################

# combine all results
rule join_results:
    input:
        genes = "{results}/function/{sample}_gene_annotation.tsv",
        tax = "{results}/taxonomy/taxmapper/{sample}_taxa_filtered.tsv"
    output:
        complete = "{results}/{sample}_complete_all.tsv",
        tax = temp("{results}/taxonomy/taxmapper/{sample}_tax.tsv")
    shell:
        """
            cut -f 2- {input.tax} > {output.tax};
            echo -e Read"\t"Uniprot-ID"\t"Uniprot-evalue"\t"Uniprot-identity"\t"Uniprot-alnlength"\t"Read-counts"\t"Sequence"\t"Gene-symbol"\t"KO-ID"\t"KEGG-pathways"\t"Taxonomy-level1"\t"Taxonomy-level2"\t"Taxonomic-lineage > {output.complete};
            paste {input.genes} {output.tax} >> {output.complete}
        """

# filter reads accoring to Uniprot e-value and only include reads with KO number and taxonomy
rule filter_joined_results:
    input:
        "{results}/{sample}_complete_all.tsv"
    output:
        "{results}/{sample}_complete_filtered_{evalue}.tsv"
    shell:
        """
            echo -e Read"\t"Uniprot-ID"\t"Uniprot-evalue"\t"Uniprot-identity"\t"Uniprot-alnlength"\t"Read-counts"\t"Sequence"\t"Gene-symbol"\t"KO-ID"\t"KEGG-pathways"\t"Taxonomy-level1"\t"Taxonomy-level2"\t"Taxonomic-lineage > {output};
            cat {input} | awk -F \"\t\" '($3<{wildcards.evalue} && $9!=\"NA\" && $12!=\"NA\"){{print $0}}' >> {output}
        """
