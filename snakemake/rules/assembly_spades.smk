# Assembly reads or sequences ##################################################

# import modules ###############################################################

import math

# rules ########################################################################

# run rnaspades for single end fasta and fastq files
if len(config['reads']) == 1 or config['format'] == "fasta":
    rule spades_single_end:
        input:
            reads = expand("{{results}}/qc/cleaned/{{sample}}_{read}.{format}.gz", read = config["reads"], format = config["format"]) if config["format"] == "fastq" else expand("{{results}}/qc/cleaned/{{sample}}.{format}.gz", format = config["format"])
        output:
            sf="{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta",
            hf="{results}/assembly/spades_{sample}/hard_filtered_transcripts.fasta",
            tr="{results}/assembly/spades_{sample}/transcripts.fasta"
        conda:
            "../envs/assemblyspades.yaml"
        params:
            memory = str(math.floor(config["maxmem"]/1024)),
            outdir_tmp = "{results}/assembly/spades_{sample}_tmp",
            outdir= "{results}/assembly/spades_{sample}_tmp"
            #type = "fq" if config["format"] == "fastq" else "fa"
        threads: config["threads"]
        shadow: "shallow"
        resources:
            mem_mb=config["maxmem"]
        log: "{results}/assembly/spades_{sample}/spades_snkmk.log"
        shell:
            """
            rnaspades.py -s {input.reads} -t {threads} -m {params.memory} -o {params.outdir_tmp} 2> {log}
            mv {params.outdir_tmp}/soft_filtered_transcripts.fasta {params.outdir}
            mv {params.outdir_tmp}/hard_filtered_transcripts.fasta {params.outdir}
            mv {params.outdir_tmp}/transcripts.fasta {params.outdir}
            rm -rf {params.outdir_tmp}
            """
# run rnaspades for paired-end data
else:
    rule spades_fastq_paired:
        input:
            reads = expand("{{results}}/qc/cleaned/{{sample}}_{read}.fastq.gz", read = config["reads"])
        output:
            sf="{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta",
            hf="{results}/assembly/spades_{sample}/hard_filtered_transcripts.fasta",
            tr="{results}/assembly/spades_{sample}/transcripts.fasta"
        conda:
            "../envs/assemblyspades.yaml"
        params:
            memory = str(math.floor(config["maxmem"]/1024)),
            outdir_tmp= "{results}/assembly/spades_{sample}_tmp",
            outdir = "{results}/assembly/spades_{sample}"
        resources:
            mem_mb=config["maxmem"]
        threads: config["threads"]
        shadow: "shallow"
        log: "{results}/assembly/spades_{sample}/spades_snkmk.log"
        shell:
            """
            rnaspades.py -1 {input.reads[0]} -2 {input.reads[1]} -t {threads} -m {params.memory} -o {params.outdir_tmp} 2> {log}
            mv {params.outdir_tmp}/soft_filtered_transcripts.fasta {params.outdir}
            mv {params.outdir_tmp}/hard_filtered_transcripts.fasta {params.outdir}
            mv {params.outdir_tmp}/transcripts.fasta {params.outdir}
            rm -rf {params.outdir_tmp}
            """
rule create_index:
    input:
        assembly = "{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta"
    output:
        index = "{results}/assembly/spades_{sample}/soft_transcripts_index_{sample}/pos.bin"
    conda:
        "../envs/assemblyspades.yaml"
    params:
        outdir = "{results}/assembly/spades_{sample}/soft_transcripts_index_{sample}"
    threads: config["threads"]
    log: "{results}/assembly/spades_{sample}/soft_transcripts_index_{sample}/indexing_smk.log"
    shell:
        """
        salmon index -t {input.assembly} -i {params.outdir} -p {threads}
        """
        
# quantify abundance single-end data
if len(config['reads']) == 1 or config['format'] == "fasta":
    rule abundance_single:
        input:
            reads = expand("{{results}}/qc/cleaned/{{sample}}_{read}.{format}.gz", read = config["reads"], format = config["format"]) if config["format"] == "fastq" else expand("{{results}}/qc/cleaned/{{sample}}.{format}.gz", format = config["format"]),
            assembly = "{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta",
            index = "{results}/assembly/spades_{sample}/soft_transcripts_index_{sample}/pos.bin"
        output:
            "{results}/assembly/quant_{sample}/quant.sf"
        conda:
            "../envs/assemblyspades.yaml"
        params:
            outdir = "{results}/assembly/quant_{sample}",
            #type = "fq" if config["format"] == "fastq" else "fa"
            indexdir = "{results}/assembly/spades_{sample}/soft_transcripts_index_{sample}"
        threads: config["threads"]
        log: "{results}/assembly/quant_{sample}/quant.log"
        shell:
            """
            salmon quant -l IU -r {input.reads} -p {threads} -o {params.outdir} -i {params.indexdir} 2> {log}
            """
# quantify abundance paired-end data
else:
    rule abundance_paired:
        input:
            reads = expand("{{results}}/qc/cleaned/{{sample}}_{read}.fastq.gz", read = config["reads"]),
            assembly = "{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta",
            index = "{results}/assembly/spades_{sample}/soft_transcripts_index_{sample}/pos.bin"
        output:
            "{results}/assembly/quant_{sample}/quant.sf"
        conda:
            "../envs/assemblyspades.yaml"
        params:
            outdir = "{results}/assembly/quant_{sample}",
            indexdir = "{results}/assembly/spades_{sample}/soft_transcripts_index_{sample}"
        threads: config["threads"]
        log: "{results}/assembly/quant_{sample}/quant.log"
        shell:
            """
             salmon quant -l IU -1 {input.reads[0]} -2 {input.reads[1]} -p {threads} -o {params.outdir} -i {params.indexdir} 2> {log}
            """

# create output files with sequences and number of reads
rule sequence_and_reads:
    input:
        assembly = "{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta",
        quant = "{results}/assembly/quant_{sample}/quant.sf"
    conda:
        "../envs/assemblyspades.yaml"
    output:temp("{results}/tmp/{sample}_reads_seqs.tsv")
    script: "../scripts/reads_seqs_file_post_assembly.py"

################################################################################
