# Get taxonomic annotation with TaxMapper ######################################

# rules ########################################################################

# unzip gz files
rule unzip:
    input:
        expand("{{results}}/qc/cleaned/{{sample}}_{read}.{format}.gz", format = config["format"], read = config["reads"]) if (config["format"] == "fastq") else expand("{{results}}/qc/cleaned/{{sample}}.{format}.gz", format = config["format"])
    output: (expand("{{results}}/qc/cleaned/{{sample}}_{read}.{format}", read = config["reads"], format = config["format"]) if (config["format"] == "fastq") else expand("{{results}}/qc/cleaned/{{sample}}.{format}", format = config["format"]))
    shell: "gunzip -k {input}"

# search taxonomy with TaxMapper
if config["assembly"]["run"]:
    # search taxonomy with Trinity fasta output
    rule search_taxonomy_assembly:
        input:
            db = "../databases/taxonomy/meta_database.db",
            reads = "{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta" if config['assembly']['method']=="Spades" else "{results}/assembly/trinity_{sample}/Trinity.fasta"
        output:
            align =("{results}/taxonomy/taxmapper/{sample}_aligned_1.aln")
        threads: config["threads"]
        params:
            out = "{results}/taxonomy/taxmapper/{sample}_aligned"
        conda: "../envs/taxonomy.yaml"
        shell:
            """
            taxmapper search -f {input.reads} -o {params.out} -t {threads} -d {input.db}
            """
else:
    # search taxonomy with fasta/q file
    rule search_taxonomy:
        input:
            db = "../databases/taxonomy/meta_database.db",
            reads = expand("{{results}}/qc/cleaned/{{sample}}.{format}", format=config["format"]) if config["format"] == "fasta" else expand("{{results}}/qc/cleaned/{{sample}}_{read}.{format}", format=config["format"], read=config["reads"])
        output:
            align = expand("{{results}}/taxonomy/taxmapper/{{sample}}_aligned_{read}.aln", read=list(map(lambda x: x.replace("R",""), config["reads"]))) if config["format"] == "fastq" else "{results}/taxonomy/taxmapper/{sample}_aligned_1.aln"
        threads: config["threads"]
        params:
            out = ("{results}/taxonomy/taxmapper/{sample}_aligned")
        conda: "../envs/taxonomy.yaml"
        script:
            "../scripts/taxmapper_search.py"

# get taxonomy for fwd/rev read(s)
rule taxmapper_map:
    input:
        seq = "{results}/tmp/{sample}_reads_seqs.tsv" if config["assembly"]["run"] or config["format"] == "fasta" else expand("{{results}}/tmp/{{sample}}_{read}_reads_seqs.tsv", read=config["reads"][0]),
        align = "{results}/taxonomy/taxmapper/{sample}_aligned_1.aln" if config["assembly"]["run"] or config["format"] == "fasta" else
        expand("{{results}}/taxonomy/taxmapper/{{sample}}_aligned_{read}.aln", read=list(map(lambda x: x.replace("R",""), config["reads"])))
    output: tax = ("{results}/taxonomy/taxmapper/{sample}_taxa.tsv")
    threads: 2
    conda: "../envs/taxonomy.yaml"
    script:
        "../scripts/taxmapper_map.py"

# filter out FP -> NA
rule taxmapper_filter:
    input:
        tax = "{results}/taxonomy/taxmapper/{sample}_taxa.tsv"
    output:
        filtered = "{results}/taxonomy/taxmapper/{sample}_taxa_filtered.tsv"
    conda: "../envs/taxonomy.yaml"
    shell:
        """
        taxmapper filter -i {input.tax} -o {output.filtered}
        """

# correct in case of assembly the occurance of taxa by read counts for that sequence
rule taxmapper_read_counts:
    input:
        tax = "{results}/taxonomy/taxmapper/{sample}_taxa_filtered.tsv",
        reads = expand("{{results}}/tmp/{{sample}}_{read}_reads_seqs.tsv", read=config["reads"][0]) if config["format"] == "fastq" and not config["assembly"]["run"] else "{results}/tmp/{sample}_reads_seqs.tsv"
    output:
        ("{results}/taxonomy/taxmapper/{sample}_taxa_readcounts.tsv")
    params:
        assembly = config["assembly"]["run"]
    resources: io=1
    conda: "../envs/taxonomy.yaml"
    script:
        "../scripts/taxmapper_read_counts.py"

# count taxonomic groups on two levels
rule taxmapper_count:
    input:
        tax = "{results}/taxonomy/taxmapper/{sample}_taxa_readcounts.tsv"
    output:
        counts1 = "{results}/taxonomy/taxmapper/{sample}_taxa_counts_level1.tsv",
        counts2 = "{results}/taxonomy/taxmapper/{sample}_taxa_counts_level2.tsv"
    conda: "../envs/taxonomy.yaml"
    shell:
        """
        taxmapper count -i {input.tax} --out1 {output.counts1} --out2 {output.counts2}
        """

# plot resulting taxonomic levels and create count tables
rule taxmapper_plot:
    input:
        counts1 = expand("{{results}}/taxonomy/taxmapper/{sample}_taxa_counts_level1.tsv", sample=config["samples"]),
        counts2 = expand("{{results}}/taxonomy/taxmapper/{sample}_taxa_counts_level2.tsv", sample=config["samples"]),
    output:
        out1 = "{results}/taxonomy/taxa_freq_norm_level1.svg",
        out2 = "{results}/taxonomy/taxa_freq_norm_level2.svg",
        out3 = "{results}/taxonomy/taxa_freq_norm_level1.tsv",
        out4 = "{results}/taxonomy/taxa_freq_norm_level2.tsv",
        out5 = "{results}/taxonomy/taxa_counts_level1.tsv",
        out6 = "{results}/taxonomy/taxa_counts_level2.tsv"
    conda: "../envs/taxonomy.yaml"
    params:
        samples = " ".join(config["samples"])
    shell:
        """
        taxmapper plot -t {input.counts1} -s {params.samples} -p {output.out1} -f {output.out3} -c {output.out5}
        taxmapper plot -t {input.counts2} -s {params.samples} -p {output.out2} -f {output.out4} -c {output.out6}
        sed -i 's/NA/0/g' {output.out5}
        sed -i 's/NA/0/g' {output.out6}
        """

# TODO: change taxmapper plot in future version and change NA to 0
