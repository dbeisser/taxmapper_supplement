# Calculate PCA and RDA on KO counts ###########################################

# TODO: include CoDa

# rules ########################################################################

rule plot_pca_KO:
    input:
        "{results}/function/KO_counts_{evalue}.tsv"
    output:
        "{results}/ordination/PCA_KO_{evalue}.pdf"
    wildcard_constraints: evalue="[-+]*\d+"
    conda: "../envs/ordination.yaml"
    script:
        "../scripts/plot_pca_KO.R"

rule color_pca_KO:
    input:
        counts = "{results}/function/KO_counts_{evalue}.tsv",
        groups = config["groups"]
    output:
        "{results}/ordination/PCA_KO_{evalue}_colored.pdf"
    conda: "../envs/ordination.yaml"
    script:
        "../scripts/color_pca_KO.R"

rule calculate_rda_KO:
    input:
        counts = "{results}/function/KO_counts_{evalue}.tsv",
        groups = config["groups"]
    output:
        full = "{results}/ordination/RDA_KO_{evalue}_full.pdf",
        sign = "{results}/ordination/RDA_KO_{evalue}_sign.pdf"
    conda: "../envs/ordination.yaml"
    script:
        "../scripts/calculate_rda_KO.R"

################################################################################
