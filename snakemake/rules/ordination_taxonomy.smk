# Calculate PCA and RDA on taxonomy counts #####################################

# TODO: include CoDa

# rules ########################################################################

rule plot_pca_taxonomy:
    input:
        "{results}/taxonomy/taxa_counts_level2.tsv"
    output:
        "{results}/ordination/PCA_taxonomy_level2.pdf"
    wildcard_constraints: evalue="[-+]*\d+"
    conda: "../envs/ordination.yaml"
    script:
        "../scripts/plot_pca_taxonomy.R"

rule color_pca_taxonomy:
    input:
        counts = "{results}/taxonomy/taxa_counts_level2.tsv",
        groups = config["groups"]
    output:
        "{results}/ordination/PCA_taxonomy_colored_level2.pdf"
    conda: "../envs/ordination.yaml"
    script:
        "../scripts/color_pca_taxonomy.R"

rule calculate_rda_taxonomy:
    input:
        counts = "{results}/taxonomy/taxa_counts_level2.tsv",
        groups = config["groups"]
    output:
        full = "{results}/ordination/RDA_taxonomy_level2_full.pdf",
        sign = "{results}/ordination/RDA_taxonomy_level2_sign.pdf"
    conda: "../envs/ordination.yaml"
    script:
        "../scripts/calculate_rda_taxonomy.R"

################################################################################
