##Modules
#Env to run gmm.hmm.euk
#mamba create -n KEGG_HMM -c bioconda -c conda-forge kofamscan perl-yaml perl-hash-merge perl-parallel-forkmanager perl-mce perl-math-utils
from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider

##Functions
FTP = FTPRemoteProvider()

##Rules:
if config["kegg_HMM"]["run"] and config["assembly"]["run"]:
    rule predict_proteins:
        input:
            assembly = "{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta" if config['assembly']['method'] == "Spades" else "{results}/assembly/trinity_{sample}/Trinity.fasta",
        params:
            sample="{sample}",
            pred_type="transdecoder",
            out_dir="{results}/assembly/spades_{sample}/"
        conda:
            "../envs/kegg.yaml"
        output:
            protein_faa="{results}/assembly/spades_{sample}/assemblyPostProcessing_dir/transcripts.cleaned.pep",
            nucleotide_fna="{results}/assembly/spades_{sample}/assemblyPostProcessing_dir/transcripts.cleaned.cds"
        threads: config["threads"]
        shell:
            """
            AssemblyPostProcessor --transcripts {input.assembly} --prediction_method {params.pred_type} ; mv -r assemblyPostProcessing_dir/ {params.out_dir}
            """

    rule create_index_predicted_genes:
        input:
            assembly="{results}/assembly/spades_{sample}/assemblyPostProcessing_dir/transcripts.cleaned.cds"
        output:
            index="{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_index/pos.bin"
        conda:
            "../envs/assemblyspades.yaml"
        params:
            outdir="{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_index"
        threads: config["threads"]
        log: "{results}/assembly/spades_{sample}/{sample}_gmhmm_nuc_index/indexing_smk.log"
        shell:
            """
            salmon index -t {input.assembly} -i {params.outdir} -p {threads}
            """

    # quantify abundance single-end data
    if len(config['reads']) == 1 or config['format'] == "fasta":
        rule abundance_single_predicted_genes:
            input:
                reads=expand("{{results}}/qc/cleaned/{{sample}}_{read}.{format}.gz",read=config["reads"],format=config[
                    "format"]) if config[
                                      "format"] == "fastq" else expand("{{results}}/qc/cleaned/{{sample}}.{format}.gz",format=
                config["format"]),
                assembly="{results}/assembly/spades_{sample}/assemblyPostProcessing_dir/transcripts.cleaned.cds",
                index="{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_index/pos.bin"
            output:
                "{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_quant/quant.sf"
            conda:
                "../envs/assemblyspades.yaml"
            params:
                outdir="{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_quant",
                #type = "fq" if config["format"] == "fastq" else "fa"
                indexdir="{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_index"
            threads: config["threads"]
            log: "{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_quant/quant.log"
            shell:
                """
                salmon quant -l IU -r {input.reads} -p {threads} -o {params.outdir} -i {params.indexdir} 2> {log}
                """
    # quantify abundance paired-end data
    else:
        rule abundance_paired_predicted_genes:
            input:
                reads=expand("{{results}}/qc/cleaned/{{sample}}_{read}.fastq.gz",read=config["reads"]),
                assembly="{results}/assembly/spades_{sample}/assemblyPostProcessing_dir/transcripts.cleaned.cds",
                index="{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_index/pos.bin"
            output:
                "{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_quant/quant.sf"
            conda:
                "../envs/assemblyspades.yaml"
            params:
                outdir="{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_quant",
                indexdir="{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_index"
            threads: config["threads"]
            log: "{results}/assembly/spades_{sample}/{sample}_trs_pred_nuc_quant/quant.log"
            shell:
                """
                 salmon quant -l IU -1 {input.reads[0]} -2 {input.reads[1]} -p {threads} -o {params.outdir} -i {params.indexdir} 2> {log}
                """
    rule download_kegg_database:
        input:
            kegg_profiles = FTP.remote("ftp.genome.jp/pub/db/kofam/profiles.tar.gz"),
            kegg_ko_list = FTP.remote("ftp.genome.jp/pub/db/kofam/ko_list.gz")
        output:
            kegg_profile_o="../databases/kofamscan_db/profiles.tar.gz",
            kegg_kolist_o="../databases/kofamscan_db/ko_list.gz",
            kegg_ko_list_e="../databases/kofamscan_db/ko_list"
        shell:
            """
            cp {input.kegg_ko_list} {output.kegg_kolist_o}
            cp {input.kegg_profiles} {output.kegg_profile_o}
            gunzip -k {output.kegg_kolist_o}
            """
    rule untar_profiles:
        input:"../databases/kofamscan_db/profiles.tar.gz"
        output:"../databases/kofamscan_db/profiles/eukaryote.hal"
        shell:"tar -xf {input} -C ../databases/kofamscan_db/"
    rule run_kegg_kofamscan:
        input:
            kegg_euk_profile="../databases/kofamscan_db/profiles/eukaryote.hal",
            kegg_ko_list_e="../databases/kofamscan_db/ko_list",
            protein_faa="{results}/assembly/spades_{sample}/assemblyPostProcessing_dir/transcripts.cleaned.pep",
        conda:"../envs/kegg.yaml"
        output: "{results}/assembly/spades_{sample}/{sample}_ko_scan_result_trs_pred.tsv"
        threads: config["threads"]
        shell:
            """
            exec_annotation -o {output} -p {input.kegg_euk_profile} -k {input.kegg_ko_list_e} --cpu={threads} -E 0.05 {input.protein_faa}
            """
    rule kegg_filter_hmm:
        input:
            kohmm_results=expand("{{results}}/assembly/spades_{sample}/{sample}_ko_scan_result_trs_pred.tsv", sample=config["samples"]),
            gene_read_quant=expand("{{results}}/assembly/spades_{sample}/{sample}_trs_pred_nuc_quant/quant.sf", sample=config["samples"])
        params:
            evalue=config["evalue"],
            pathway_map=config["KEGG_pathways"],
            pathway_pedia=config["KEGG_pathway_pedia"]
        output:
            ko_profile="{results}/function/KO_counts_HMM_version_trs_pred_{evalue}.tsv",
            pathway_profile="{results}/function/KEGG_Pathway_profile_HMM_trs_pred_{evalue}.tsv",
            map_dict="{results}/../databases/kegg_pathway_map_dict_trs_pred_{evalue}.tsv"
        script: "../scripts/kegg_hmm_processing.py"