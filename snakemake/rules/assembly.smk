# Assembly reads or sequences ##################################################

# import modules ###############################################################

import math

# rules ########################################################################

# run trinity for single end fasta and fastq files
if len(config['reads']) == 1 or config['format'] == "fasta":
    rule trinity_single_end:
        input:
            reads = expand("{{results}}/qc/cleaned/{{sample}}_{read}.{format}.gz", read = config["reads"], format = config["format"]) if config["format"] == "fastq" else expand("{{results}}/qc/cleaned/{{sample}}.{format}.gz", format = config["format"])
        output:
            "{results}/assembly/trinity_{sample}/Trinity.fasta"
        conda:
            "../envs/assembly.yaml"
        params:
            memory = str(math.floor(config["maxmem"]/1024))+"G",
            outdir = "{results}/assembly/trinity_{sample}",
            type = "fq" if config["format"] == "fastq" else "fa"
        threads: config["threads"]
        resources:
            mem_mb=config["maxmem"]
        log: "{results}/assembly/trinity_{sample}/Trinity.log"
        shell:
            """
            Trinity --seqType {params.type} --single {input.reads} --CPU {threads} --max_memory {params.memory} --output {params.outdir} 2> {log}
            """
# run trinity for paired-end data
else:
    rule trinity_fastq_paired:
        input:
            reads = expand("{{results}}/qc/cleaned/{{sample}}_{read}.fastq.gz", read = config["reads"])
        output:
            "{results}/assembly/trinity_{sample}/Trinity.fasta"
        conda:
            "../envs/assembly.yaml"
        params:
            memory = str(math.floor(config["maxmem"]/1024))+"G",
            outdir = "{results}/assembly/trinity_{sample}"
        resources:
            mem_mb=config["maxmem"]
        threads: config["threads"]
        log: "{results}/assembly/trinity_{sample}/Trinity.log"
        shell:
            """
            Trinity --seqType fq --left {input.reads[0]} --right {input.reads[1]} --CPU {threads} --max_memory {params.memory} --output {params.outdir} 2> {log}
            """

# quantify abundance single-end data
if len(config['reads']) == 1 or config['format'] == "fasta":
    rule abundance_single:
        input:
            reads = expand("{{results}}/qc/cleaned/{{sample}}_{read}.{format}.gz", read = config["reads"], format = config["format"]) if config["format"] == "fastq" else expand("{{results}}/qc/cleaned/{{sample}}.{format}.gz", format = config["format"]),
            assembly = "{results}/assembly/trinity_{sample}/Trinity.fasta"
        output:
            "{results}/assembly/quant_{sample}/quant.sf"
        conda:
            "../envs/assembly.yaml"
        params:
            outdir = "{results}/assembly/quant_{sample}",
            type = "fq" if config["format"] == "fastq" else "fa"
        threads: config["threads"]
        log: "{results}/assembly/quant_{sample}/quant.log"
        shell:
            """
            align_and_estimate_abundance.pl --transcripts {input.assembly} --seqType {params.type} --single {input.reads} --est_method salmon --trinity_mode --prep_reference --output_dir {params.outdir} --thread_count {threads} 2> {log}
            """
# quantify abundance paired-end data
else:
    rule abundance_paired:
        input:
            reads = expand("{{results}}/qc/cleaned/{{sample}}_{read}.fastq.gz", read = config["reads"]),
            assembly = "{results}/assembly/trinity_{sample}/Trinity.fasta"
        output:
            "{results}/assembly/quant_{sample}/quant.sf"
        conda:
            "../envs/assembly.yaml"
        params:
            outdir = "{results}/assembly/quant_{sample}"
        threads: config["threads"]
        log: "{results}/assembly/quant_{sample}/quant.log"
        shell:
            """
            align_and_estimate_abundance.pl --transcripts {input.assembly} --seqType fq --left {input.reads[0]} --right {input.reads[1]} --est_method salmon --trinity_mode --prep_reference --output_dir {params.outdir} --thread_count {threads} 2> {log}
            """

# create output files with sequences and number of reads

rule sequence_and_reads:
    input:
        assembly = "{results}/assembly/trinity_{sample}/Trinity.fasta",
        quant = "{results}/assembly/quant_{sample}/quant.sf"
    conda:
        "../envs/assemblyspades.yaml"
    output:temp("{results}/tmp/{sample}_reads_seqs.tsv")
    script: "../scripts/reads_seqs_file_post_assembly.py"        
        
################################################################################
