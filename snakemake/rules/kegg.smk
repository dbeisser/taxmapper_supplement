# add KEGG IDs and pathways

# output: 0: read, 1: Uniprot ID, 2: log10-evalue, 3: identity, 4: aln-length, 5: sequence, 6:gene-name, 7:KO-ID, 8:pathways
rule get_KEGG_info:
    input:
        tsv = "{results}/function/uniprot/{sample}_best_uniprot.tsv",
        u2gene = "../databases/uniprot/uniprot2gene.h5",
        u2ko = "../databases/uniprot/uniprot2ko.h5" if config["KO_list_file"] != "" else [],
        ko2pw = "../databases/uniprot/ko2pathway.h5" if config["KEGG_pathways"] != "" else []
    output:
        "{results}/function/{sample}_gene_annotation.tsv"
    resources: io=3
    conda: "../envs/kegg.yaml"
    script:
        "../scripts/get_KEGG_info.py"

rule KEGG_filter:
    input:
        expand("{{results}}/function/{sample}_gene_annotation.tsv", sample=config["samples"])
    output:
        "{results}/function/KO_counts_{evalue}.tsv"
    script:
        "../scripts/KEGG_filter.py"

#rule KEGG_count_matrix:
#    input:
#        expand("{{results}}/function/{sample}_gene_annotation_{{evalue}}.tmp", sample=config["samples"])
#    output:
#        "{results}/function/KO_counts_{evalue}.tsv"
#    params:
#        samples = config["samples"]
#    conda: "../envs/R.yaml"
#    script:
#        "../scripts/KEGG_count_matrix.R"
#
