# Get Uniprot functional annotation ############################################

#modules


# functions ####################################################################

def getBestHit(infile_a, infile_b, outfile):
    """Write best hit accoring to e-value for forward and reverse file."""
    with open(infile_a, 'rt') as a, open(infile_b, 'rt') as b, open(outfile, 'wt') as c:
        for linea,lineb in zip(a,b):
            a_vec = linea.split("\t")
            b_vec = lineb.split("\t")
            # 0: query id, 1: subject id, 2:log e-value
            if (float(a_vec[2]) <= float(b_vec[2])):
                c.write("\t".join(a_vec))
            elif (float(b_vec[2]) < float(a_vec[2])):
                c.write("\t".join(b_vec))
            else:
                print("Something is wrong!")

# rules ########################################################################

if config["assembly"]["run"]:
    # search uniprot with Trinity fasta output
    rule search_uniprot_assembly:
        input:
            uniprotdb = "../databases/uniprot/uniprot_sprot.db",
            reads = "{results}/assembly/spades_{sample}/soft_filtered_transcripts.fasta" if config['assembly']['method']=="Spades" else "{results}/assembly/trinity_{sample}/Trinity.fasta"
        output:
            align = "{results}/function/uniprot/{sample}_aligned.aln"
        threads: config["threads"]
        params:
            out = "{results}/function/uniprot/{sample}_aligned"
        conda: "../envs/uniprot.yaml"
        shell:
            """
            cat {input.reads} | rapsearch -q stdin -d {input.uniprotdb} -o {params.out} -z {threads} -b 1 -v 0 -p T -t n -e 1
            """
else:
    # search uniprot with fastq/a file
    rule search_uniprot:
        input:
            uniprotdb = "../databases/uniprot/uniprot_sprot.db",
            reads = expand("{{results}}/qc/cleaned/{{sample}}.{format}.gz", format=config["format"]) if config["format"] == "fasta" else expand("{{results}}/qc/cleaned/{{sample}}_{{read}}.{format}.gz", format=config["format"])
        output:
            align = "{results}/function/uniprot/{sample}_{read}_aligned.aln" if config["format"] == "fastq" else "{results}/function/uniprot/{sample}_aligned.aln"
        threads: config["threads"]
        params:
            format = "q" if config["format"] == "fastq" else "n",
            out = "{results}/function/uniprot/{sample}_{read}_aligned" if config["format"] == "fastq" else "{results}/function/uniprot/{sample}_aligned"
        conda: "../envs/uniprot.yaml"
        shell:
            """
            zcat {input.reads} | rapsearch -q stdin -d {input.uniprotdb} -o {params.out} -z {threads} -b 1 -v 0 -p T -t {params.format} -e 1
            """

# create shorter output 0: read, 1: Uniprot ID, 2: log10-evalue, 3: identity, 4: aln-length, m8 does not contain unmapped reads!
rule tmp_output_uniprot:
    input:
        align = "{results}/function/uniprot/{sample}_{read}_aligned.aln" if config["format"] == "fastq" and not config["assembly"]["run"] else "{results}/function/uniprot/{sample}_aligned.aln"
    output:
        tmp = temp("{results}/function/uniprot/{sample}_{read}_aligned.tmp" if config["format"] == "fastq" and not config["assembly"]["run"] else "{results}/function/uniprot/{sample}_aligned.tmp")
    resources: io = 1
    run:
        if config["assembly"]["run"]:
            id = "NODE_" #if config['assembly']['method']=="Spades" else "TRINITY_"
            shell("grep {id} {input} | awk \'BEGIN {{FS=\"[> |=\\t]\"}} {{sub(/^$/,\"NA\",$4); sub(/^$/,100,$9); sub(/^$/,\"NA\",$11); sub(/%/,\"\",$11); sub(/^$/,\"NA\",$13); print $1\"\\t\"$4\"\\t\"$9\"\\t\"$11\"\\t\"$13}}\' > {output}")
        # fastq and fasta look the same
        else:
            shell("ID=$(head -n 1 {input} | cut -d : -f1);grep $ID {input} | awk \'BEGIN {{FS=\"[> |=\\t]\"}} {{sub(/^$/,\"NA\",$4); sub(/^$/,100,$9); sub(/^$/,\"NA\",$11); sub(/%/,\"\",$11); sub(/^$/,\"NA\",$13); print $1\"\\t\"$4\"\\t\"$9\"\\t\"$11\"\\t\"$13}}\' > {output}")

if not config["assembly"]["run"]:
    # get read sequences and counts of 1 if assembly was not performed
    rule get_sequence:
        input:
            expand("{{results}}/qc/cleaned/{{sample}}.{format}.gz", format=config["format"]) if config["format"] == "fasta" else expand("{{results}}/qc/cleaned/{{sample}}_{{read}}.{format}.gz", format=config["format"])
        output:
            counts = temp("{results}/tmp/{sample}_{read}_reads_seqs.tsv" if config["format"] == "fastq" else "{results}/tmp/{sample}_reads_seqs.tsv")
        conda: "../envs/uniprot.yaml"
        params: format = config["format"]
        script: "../scripts/get_sequence.py"

# combine short files uniprot hits and sequence
# 0: read, 1: Uniprot ID, 2: log10-evalue, 3: identity, 4: aln-length, 4: num-reads, 5: sequence
rule short_output_uniprot:
    input:
        reads = "{results}/function/uniprot/{sample}_{read}_aligned.tmp" if config["format"] == "fastq" and not config["assembly"]["run"] else "{results}/function/uniprot/{sample}_aligned.tmp",
        counts = "{results}/tmp/{sample}_{read}_reads_seqs.tsv" if config["format"] == "fastq" and not config["assembly"]["run"] else "{results}/tmp/{sample}_reads_seqs.tsv"
    output:
        temp("{results}/function/uniprot/{sample}_{read}_uniprot.short" if config["format"] == "fastq" and not config["assembly"]["run"] else "{results}/function/uniprot/{sample}_uniprot.short")
    resources: io=1
    shell: "awk \'FNR==NR{{a[$1]=$2\"\\t\"$3;next;}} {{print $0\"\\t\"a[$1];}}\' {input.counts} {input.reads}  > {output}"

# get best hit from forward and reverse read
rule get_best_hit_uniprot:
    input:
        expand("{{results}}/function/uniprot/{{sample}}_{read}_uniprot.short", read=config["reads"]) if config["format"] == "fastq" and not config["assembly"]["run"] else "{results}/function/uniprot/{sample}_uniprot.short"
    output: out = "{results}/function/uniprot/{sample}_best_uniprot.tsv"
    run:
        if len(input) == 2:
            getBestHit(input[0], input[1], output.out)
        else:
            shell("cp {input[0]} {output.out}")

################################################################################
