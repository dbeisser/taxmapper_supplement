# Perform GAGE pathway anaylsis und visualisation ##############################

# rules ########################################################################

# According to GAGE Vignette: RNA-Seq Data Pathway and Gene-set Analysis Workflows
rule GAGE_pathway_visualisation:
    input:
        groups = config["groups"],
        counts = "{results}/function/KO_counts_{evalue}.tsv"
    output:
        "{results}/pathways/done_{evalue}.txt"
    params:
        cutoff=config["FDR"]
    conda: "../envs/pathway_analysis.yaml"
    script:
        "../scripts/GAGE_pathway_visualisation.R"

# According to GAGE Vignette: RNA-Seq Data Pathway and Gene-set Analysis Workflows
rule GAGE_pathway_analysis:
    input:
        counts = "{results}/function/KO_counts_{evalue}.tsv",
        groups = config["groups"]
    output:
        "{results}/pathways/GAGE_pathways_KO_{evalue}.tsv"
    params:
        cutoff=config["FDR"]
    conda: "../envs/pathway_analysis.yaml"
    script:
        "../scripts/GAGE_pathway_analysis.R"

################################################################################
