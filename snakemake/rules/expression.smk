# Calculate diff. expression between def. groups and perform enrichment ########

# rules ########################################################################

rule calculate_diff_expression:
    input:
        counts = "{results}/function/KO_counts_{evalue}.tsv",
        groups = config["groups"]
    output:
        "{results}/expression/diff_exp_KO_{evalue}.tsv"
    params:
        cutoff=config["FDR"],
        formula=config["model"]
    conda: "../envs/expression.yaml"
    script:
        "../scripts/calculate_diff_expression.R"

rule KEGG_enrichment:
    input:
        counts = "{results}/function/KO_counts_{evalue}.tsv",
        exp = "{results}/expression/diff_exp_KO_{evalue}.tsv"
    output:
        enrichment = "{results}/expression/KEGG_enrichment_KO_{evalue}.tsv"
    params:
        cutoff=config["evalue"]
    conda: "../envs/expression.yaml"
    script:
        "../scripts/KEGG_enrichment.R"

################################################################################
