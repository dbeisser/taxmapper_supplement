#!/bin/bash

echo Please enter the name of the config file \(default: config.yaml\)
read config
if [[ -z "$config" ]];
then
  config="config.yaml"
fi

env_loc=$(conda info --base)/etc/profile.d/conda.sh
source $env_loc
conda activate snakemake

cores=$(grep "cores: " $config | awk '{print $2}')
maxmem=$(grep "maxmem: " $config | awk '{print $2}')
io=$(grep "io: " $config | awk '{print $2}')

snakemake -s snakefile.smk --use-conda --configfile $config --cores $cores --resources maxmem=$maxmem io=$io -r
