#Import Statements

# Analyze metatranscriptome sequences ##########################################

# include subworkflows #########################################################

# subworkflow to create Uniprot database and mapping files
module uniprot:
    prefix: "../databases/uniprot/"
    snakefile: "../databases/uniprot/create_uniprot_db.smk"
    config: config
use rule * from uniprot as uniprot_module_*



# MOdule to create KEGG pathway mapping file
module kegg:
    prefix: "../databases/uniprot/"
    snakefile: "../databases/uniprot/create_kegg_mapping.smk"
    config: config
use rule * from kegg as kegg_module_*


# subworkflow to create RAPSerach taxonomy database index
module taxonomy:
    prefix: "../databases/taxonomy/"
    snakefile: "../databases/taxonomy/create_taxonomy_db.smk"
    config: config
use rule * from taxonomy as taxonomy_module_*



# rules ########################################################################

# pre-processing
include: "rules/preprocessing.smk"

# optionally run assembly
if config['assembly']['run']:
    include: "rules/assembly_spades.smk" if config['assembly']['method']=="Spades" else "rules/assembly.smk"

# functional annotation
include: "rules/uniprot.smk"
include: "rules/kegg.smk"

# taxonomic annotation
include: "rules/taxonomy.smk"

# join functional and taxonomic annotation
include: "rules/join.smk"

#optionally run KEGG_KOFamScan on spades assembled scaffolds (might work with trinity but not tested
if config['kegg_HMM']['run']:
    include: "rules/KEGG_HMM2.smk"
# optionally run differential gene expression
if config['expression']['run']:
    include: "rules/expression.smk"

# optionally run ordination on KOs and taxonomy
if config['ordination']['run']:
    include: "rules/ordination_ko.smk"
    include: "rules/ordination_taxonomy.smk"

# optionally run pathways analysis
if config['pathway_analysis']['run']:
    include: "rules/pathway_analysis.smk"

# rule all lists all final output files from each step #########################
rule all:
    input:
        #database outputs
        rules.uniprot_module_all.input,
        rules.kegg_module_all.input,
        rules.taxonomy_module_all.input,
        # output preprocessing
        expand("{results}/qc/multiqc_report.html", results=config["results"]) if (config["format"] == "fastq") else [],
        expand("{results}/qc/cleaned/{sample}_{read}.{format}.gz", results=config["results"], sample=config["samples"], read = config["reads"], format = config["format"]) if config["format"] == "fastq" else expand("{results}/qc/cleaned/{sample}.{format}.gz", results=config["results"], sample=config["samples"], format = config["format"]),
        # output assembly
        expand("{results}/assembly/quant_{sample}/quant.sf", results=config["results"], sample=config["samples"]) if config["assembly"]["run"] else [],
        # output functional annotation
        expand("{results}/function/uniprot/{sample}_best_uniprot.tsv", results=config["results"], sample=config["samples"]),
        expand("{results}/function/{sample}_gene_annotation.tsv", results=config["results"], sample=config["samples"]),
        expand("{results}/function/KO_counts_{evalue}.tsv", results=config["results"], evalue=config["log_evalue"]),
        expand("{results}/function/KEGG_Pathway_profile_HMM_{evalue}.tsv",results=config["results"], evalue=config["evalue"]) if config['kegg_HMM']['run'] else [],
        # output taxonomic annotation
        expand("{results}/taxonomy/taxmapper/{sample}_taxa_filtered.tsv", results=config["results"], sample=config["samples"]),
        expand("{results}/taxonomy/taxa_counts_level1.tsv", results=config["results"]),
        expand("{results}/taxonomy/taxa_counts_level2.tsv", results=config["results"]),
        # joined output
        expand("{results}/{sample}_complete_all.tsv", results=config["results"], sample=config["samples"]),
        expand("{results}/{sample}_complete_filtered_{evalue}.tsv", results=config["results"], sample=config["samples"], evalue=config["log_evalue"]),
        # output kegg_kofamscan results #as of now configured only for spades output.
        #expand("{results}/assembly/spades_{sample}/{sample}_gmhmm_prot.faa", results=config["results"], sample=config["samples"]) if config["kegg_HMM"]["run"] else [],
        #expand("{results}/assembly/spades_{sample}/{sample}_gmhmm_nuc_quant/quant.sf", results=config["results"], sample=config["samples"]) if config["kegg_HMM"]["run"] else [],
        expand("{results}/function/KEGG_Pathway_profile_HMM_trs_pred_{evalue}.tsv", results =config["results"], evalue=config["evalue"])if config["kegg_HMM"]["run"] else [],
        # output expression analysis
        expand("{results}/expression/diff_exp_KO_{evalue}.tsv", results=config["results"], evalue=config["log_evalue"]) if config["expression"]["run"] else [],
        expand("{results}/expression/KEGG_enrichment_KO_{evalue}.tsv", results=config["results"], evalue=config["log_evalue"]) if config["expression"]["run"] else[],
        # output ordination KO
        expand("{results}/ordination/PCA_KO_{evalue}_colored.pdf", results=config["results"], evalue=config["log_evalue"]) if config["ordination"]["run"] else [],
        expand("{results}/ordination/RDA_KO_{evalue}_sign.pdf", results=config["results"], evalue=config["log_evalue"]) if config["ordination"]["run"] else [],
        # output ordination taxonomy
        expand("{results}/ordination/PCA_taxonomy_colored_level2.pdf", results=config["results"]) if config["ordination"]["run"] else [],
        expand("{results}/ordination/RDA_taxonomy_level2_sign.pdf", results=config["results"]) if config["ordination"]["run"] else [],
        # output pathway analysis
        expand("{results}/pathways/done_{evalue}.txt", results=config["results"], evalue=config["log_evalue"]) if config["pathway_analysis"]["run"] else [],
        expand("{results}/pathways/GAGE_pathways_KO_{evalue}.tsv", results=config["results"], evalue=config["log_evalue"]) if config["pathway_analysis"]["run"] else []
    default_target: True




# status messages ##############################################################

onsuccess:
    print("Workflow finished, no error")

onerror:
    print("An error occurred")
    shell("cat {log}")

################################################################################
