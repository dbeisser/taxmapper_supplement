# Change Log

## 2021-08-30

### Changed
- updated packages
- release **version 2.2**

## 2020-11-11

### Changed
- release **version 2.1**

## 2020-05-18

### Changed
- included fastq and fq as input file format
- added forgotten fastqc input to rule all
- output file structure

### Added
- multiqc report for fastq files

## 2020-04-23

### Changed
- finished fasta, fastq single and paired-end input options with and w/o assemby
- release **version 2.0**

## 2020-04-17

### Changed
- adapted all rules that depend on assembled file
- made certain rules optional
- adapted file structure and included: rules, env and script directories
- adapted config file

### Added
- included assembly option
- added fasta as input format
- bash run.sh script
- conda environments for all rules
- added rules for blocks of rules
- model formula for expression analysis in config
