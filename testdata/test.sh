#!/bin/bash

# create raw data directory if it doesn't exist
if [ ! -f fastq/test1_R1.fastq ]
	then
		cp -r raw fastq
		gunzip fastq/*.fastq.gz
fi

# create rapsearch index if it doesn't exist
if [ ! -f ../databases/taxonomy/meta_database.db ]
	then
		gunzip -c ../databases/taxonomy/meta_database.fa.gz > ../databases/taxonomy/meta_database.fasta
		prerapsearch -d ../databases/taxonomy/meta_database.fasta -n ../databases/taxonomy/meta_database.db
fi

# run taxmapper
taxmapper run -d ../databases/taxonomy/meta_database.db -m 100 -f fastq -t 4
