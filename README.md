# Taxmapper Supplement

This repository contains the Taxmapper database and an example workflow.

To run TaxMapper on the test data:

- Install Taxmapper (detailed info: [here](https://bitbucket.org/dbeisser/taxmapper)) and clone or download the content of this repository:
- navigate to the `testdata` folder
- run the `test.sh` script which unpacks the data and runs taxmapper
```shell
$ conda create -c conda-forge -c bioconda -n taxmapper taxmapper rapsearch=2.24
$ git clone git@bitbucket.org:dbeisser/taxmapper_supplement.git
$ cd taxmapper_supplement/testdata
$ sh test.sh
```

To run the complete Taxmapper snakemake pipeline:

- create a conda environment called snakemake as described [here](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html): `$ conda create -c conda-forge -c bioconda -n snakemake snakemake`
- clone or download the content of this repository
```shell
$ git clone git@bitbucket.org:dbeisser/taxmapper_supplement.git
$ cd taxmapper_supplement
```

- navigate to the `snakemake` folder
- adjust the `config.yaml` file according to your data
- run the workflow with `$ ./run.sh`

The first run may take quite a while due to the download and creation of the taxonomic and functional databases if they do not exist yet.

For a more detailed tutorial and installation guide, please take a look at the [main Taxmapper repository](https://bitbucket.org/dbeisser/taxmapper).

## Taxmapper Database

The TaxMapper database can be found [here](https://bitbucket.org/dbeisser/taxmapper_supplement/src/master/databases/taxonomy).

It includes the following taxonomic groups.
For informations on included organisms see  [BioNet paper](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-017-4168-6).

| Supergroup     | Group              | Number of taxa |
|----------------|--------------------|----------------|
| Alveolata      | Apicomplexa        | 4              |
| Alveolata      | Chromerida         | 2              |
| Alveolata      | Ciliophora         | 8              |
| Alveolata      | Dinophyceae        | 11             |
| Alveolata      | Perkinsea          | 1              |
| Amorphea       | Amoebozoa          | 7              |
| Amorphea       | Apusozoa           | 1              |
| Amorphea       | Choanoflagellida   | 2              |
| Amorphea       | Fungi              | 6              |
| Amorphea       | Metazoa            | 9              |
| Amorphea       | Opisthokonta Rest  | 2              |
| Archaeplastida | Chlorophyta        | 12             |
| Archaeplastida | Glaucocystophyceae | 2              |
| Archaeplastida | Rhodophyta         | 3              |
| Archaeplastida | Streptophyta       | 5              |
| Excavata       | Euglenozoa         | 4              |
| Excavata       | Fornicata          | 2              |
| Excavata       | Heterolobosea      | 2              |
| Excavata       | Parabasalia        | 1              |
| Hacrobia       | Cryptophyta        | 4              |
| Hacrobia       | Haptophyta         | 7              |
| Rhizaria       | Cercozoa           | 3              |
| Rhizaria       | Foraminifera       | 4              |
| Stramenopile   | Bacillariophyta    | 15             |
| Stramenopile   | Bigyra             | 4              |
| Stramenopile   | Chrysophyceae      | 6              |
| Stramenopile   | Pseudofungi        | 3              |
| Stramenopile   | Stramenopile Rest  | 12             |

## How to Cite

Beisser D, Graupner N, Grossmann L, Timm H, Boenigk J, Rahmann S. TaxMapper: an analysis tool, reference database and workflow for metatranscriptome analysis of eukaryotic microorganisms. BMC Genomics. 2017 Oct 16;18(1):787. doi:10.1186/s12864-017-4168-6.